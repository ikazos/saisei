use std::{collections::HashMap, f32::consts::PI, fs::File, io::{self, Read}, path::{Path, PathBuf}, time::Duration};

use cpal::{Device, StreamConfig, Stream, SizedSample, FromSample, traits::{DeviceTrait, HostTrait, StreamTrait}};
use crossterm::event::{self, Event, KeyCode};
use ratatui::{backend::{Backend, CrosstermBackend}, layout::{Constraint, Direction, Layout}, style::Stylize, widgets::Paragraph, Frame, Terminal};
use argh::FromArgs;
use rtrb::{Producer, Consumer, RingBuffer};
use serde::{Serialize, Deserialize};
use symphonia::core::{audio::{AudioBuffer, Signal}, codecs::{CODEC_TYPE_NULL, DecoderOptions}, formats::FormatOptions, io::MediaSourceStream, meta::MetadataOptions, probe::Hint};



/*  Constants */

const MAIN_LOOP_SLEEP_DUR: u64 = 3;
const AUDIO_OUTPUT_BUFFER_SIZE: usize = 512;
const MSG_RINGBUFFER_SIZE: usize = 8;

const BIQUAD_F0_MIN: f32 = 100.;
const BIQUAD_F0_MAX: f32 = 22000.;

const BIQUAD_Q_MIN: f32 = 0.1;
const BIQUAD_Q_MAX: f32 = 3.;



/*  Type aliases */

type TwoChannelBuffer = [Vec<f32>; 2];



/*  Command-line arguments */

#[derive(FromArgs)]
#[derive(Debug, Clone)]
/// yet another music player
struct Args {
    /// playlist
    #[argh(positional)]
    playlist: PathBuf,
}



#[derive(Serialize, Deserialize)]
#[derive(Debug, Clone)]
struct Playlist {
    name: String,
    paths: Vec<PathBuf>,
}



/*  Messages */

#[derive(Debug)]
enum AudioThreadMessage {
    SourceInsertBuffer { key: usize, buffer: TwoChannelBuffer },
    SourceRemoveBuffer { key: usize },
    SourceSetCurrentBufferKey { key: usize },
    SourceSetCurrentBufferSampleIdxs { idxs: [usize; 2] },
    SourceSetIsPlaying { is_playing: bool },
    BiquadSetF0 { f0: f32 },
    BiquadSetQ { q: f32 },
}



#[derive(Debug)]
enum MainThreadMessage {
    SourceSnapshot { is_current_buffer_complete: bool, is_playing: bool, },
    BiquadSnapshot { f0: f32, q: f32 },
}



/*  Modules */

#[derive(Debug)]
struct Source {
    audio_buffers: HashMap<usize, TwoChannelBuffer>,
    curr_buffer_key: usize,
    curr_buffer_sample_idxs: [usize; 2],
    is_curr_buffer_complete: bool,
    is_playing: bool,
}



impl Default for Source {
    fn default() -> Self {
        Self {
            audio_buffers: HashMap::new(),
            curr_buffer_key: 0,
            curr_buffer_sample_idxs: [ 0, 0 ],
            is_curr_buffer_complete: false,
            is_playing: true,
        }
    }
}



#[derive(Debug)]
struct Biquad {
    f0: f32,
    q: f32,
}



impl Default for Biquad {
    fn default() -> Self {
        Self { f0: 800., q: 0.7 }
    }
}



/// Process.
fn process_audio(
    temp_buffers: &mut [[f32; AUDIO_OUTPUT_BUFFER_SIZE]; 2],
    output_buffers: &mut [[f32; AUDIO_OUTPUT_BUFFER_SIZE]; 2],
    biquad_lagged_input_buffers: &mut [[f32; 2]; 2],
    biquad_lagged_output_buffers: &mut [[f32; 2]; 2],
    sample_rate: f32,
    source: &mut Source,
    biquad: &Biquad,
) {
    /*  source */

    if source.is_playing {
        let curr_buffer = source.audio_buffers.get(&source.curr_buffer_key).expect("Buffer not found");

        //  Copy from sources into buffers.
        let num_samples: [usize; 2] = [
            AUDIO_OUTPUT_BUFFER_SIZE.min(curr_buffer[0].len() - source.curr_buffer_sample_idxs[0]),
            AUDIO_OUTPUT_BUFFER_SIZE.min(curr_buffer[1].len() - source.curr_buffer_sample_idxs[1]),
        ];

        let srcs = [
            &curr_buffer[0][source.curr_buffer_sample_idxs[0]..source.curr_buffer_sample_idxs[0] + num_samples[0]],
            &curr_buffer[1][source.curr_buffer_sample_idxs[1]..source.curr_buffer_sample_idxs[1] + num_samples[1]],
        ];

        let (dst0, zero_dst0) = temp_buffers[0].split_at_mut(num_samples[0]);
        dst0.copy_from_slice(srcs[0]);
        zero_dst0.fill(0.);

        let (dst1, zero_dst1) = temp_buffers[1].split_at_mut(num_samples[1]);
        dst1.copy_from_slice(srcs[1]);
        zero_dst1.fill(0.);

        source.curr_buffer_sample_idxs[0] += num_samples[0];
        source.curr_buffer_sample_idxs[1] += num_samples[1];

        //  If we ran out of samples halfway, zero out the remaining samples and stop playback.
        if
            source.curr_buffer_sample_idxs[0] >= curr_buffer[0].len() &&
            source.curr_buffer_sample_idxs[1] >= curr_buffer[1].len()
        {
            source.is_curr_buffer_complete = true;
            source.is_playing = false;
        }
    }
    else {
        temp_buffers[0].fill(0.);
        temp_buffers[1].fill(0.);
    }



    /*  biquad */

    //  Update the output buffer.
    let w0: f32 = 2. * PI * biquad.f0 / sample_rate;
    let cos_w0: f32 = w0.cos();
    let sin_w0: f32 = w0.sin();
    let alpha: f32 = sin_w0 / (2. * biquad.q);
    let b0: f32 = (1. - cos_w0) / 2.;
    let b1: f32 = 1. - cos_w0;
    let b2: f32 = (1. - cos_w0) / 2.;
    let a0: f32 = 1. + alpha;
    let a1: f32 = -2. * cos_w0;
    let a2: f32 = 1. - alpha;

    for chan in 0..output_buffers.len() {
        for idx in 0..output_buffers[chan].len() {
            let xn1 = match idx >= 1 {
                true => temp_buffers[chan][idx - 1],
                false => biquad_lagged_input_buffers[chan][idx + 1],
            };
            let xn2 = match idx >= 2 {
                true => temp_buffers[chan][idx - 2],
                false => biquad_lagged_input_buffers[chan][idx],
            };
            let yn1 = match idx >= 1 {
                true => output_buffers[chan][idx - 1],
                false => biquad_lagged_output_buffers[chan][idx + 1],
            };
            let yn2 = match idx >= 2 {
                true => output_buffers[chan][idx - 2],
                false => biquad_lagged_output_buffers[chan][idx],
            };

            output_buffers[chan][idx] =
                (b0 / a0) * temp_buffers[chan][idx] +
                (b1 / a0) * xn1 +
                (b2 / a0) * xn2 -
                (a1 / a0) * yn1 -
                (a2 / a0) * yn2;
        }
    }

    //  Update the lagged input buffers.
    biquad_lagged_input_buffers[0].copy_from_slice(&temp_buffers[0][temp_buffers[0].len() - 2..]);
    biquad_lagged_input_buffers[1].copy_from_slice(&temp_buffers[1][temp_buffers[1].len() - 2..]);

    //  Update the lagged output buffers.
    biquad_lagged_output_buffers[0].copy_from_slice(&output_buffers[0][output_buffers[0].len() - 2..]);
    biquad_lagged_output_buffers[1].copy_from_slice(&output_buffers[1][output_buffers[1].len() - 2..]);



    /*  limiter */
}



/// Returns sample size and stream.
fn make_stream_biquad_limiter<T>(
    device: &Device,
    config: &StreamConfig,
    mut audio_msg_rx: Consumer<AudioThreadMessage>,
    mut main_msg_tx: Producer<MainThreadMessage>,
) -> Result<Stream, ()>
    where T: SizedSample + FromSample<f32>
{
    let num_channels = config.channels as usize;
    let sample_rate = config.sample_rate.0 as f32;

    //  Temporary buffers for audio processing.
    let mut temp_buffers: [[f32; AUDIO_OUTPUT_BUFFER_SIZE]; 2] = [
        [0.; AUDIO_OUTPUT_BUFFER_SIZE],
        [0.; AUDIO_OUTPUT_BUFFER_SIZE],
    ];

    //  Output buffers, i.e. this goes to the audio callback.
    let mut output_buffers: [[f32; AUDIO_OUTPUT_BUFFER_SIZE]; 2] = [
        [0.; AUDIO_OUTPUT_BUFFER_SIZE],
        [0.; AUDIO_OUTPUT_BUFFER_SIZE],
    ];

    let mut output_buffer_idxs: [usize; 2] = [ 0, 0 ];

    //  2 pairs of lagged buffers for biquad:
    //  1 pair for lagged input, 1 pair for lagged output.
    let mut biquad_lagged_input_buffers: [[f32; 2]; 2] = [ [0.; 2], [0.; 2] ];
    let mut biquad_lagged_output_buffers: [[f32; 2]; 2] = [ [0.; 2], [0.; 2] ];

    //  Modules.
    let mut source = Source::default();
    let mut biquad = Biquad::default();

    let stream = device.build_output_stream(
        config,
        move |data: &mut [T], _: &cpal::OutputCallbackInfo| {
            //  Simple assertion...
            assert!(data.len() % num_channels == 0);

            /*  Process messages. */
            if let Ok(msg) = audio_msg_rx.pop() {
                match msg {
                    AudioThreadMessage::SourceInsertBuffer { key, buffer } => {
                        assert!(source.audio_buffers.insert(key, buffer).is_none());
                    },

                    AudioThreadMessage::SourceRemoveBuffer { key } => {
                        assert!(source.audio_buffers.remove(&key).is_some());
                    },

                    AudioThreadMessage::SourceSetCurrentBufferKey { key } => {
                        source.curr_buffer_key = key;
                    },

                    AudioThreadMessage::SourceSetCurrentBufferSampleIdxs { idxs } => {
                        source.curr_buffer_sample_idxs = idxs;
                        source.is_curr_buffer_complete = false;
                        main_msg_tx.push(MainThreadMessage::SourceSnapshot { is_current_buffer_complete: source.is_curr_buffer_complete, is_playing: source.is_playing }).expect("audio: can't push ss msg");
                    },

                    AudioThreadMessage::SourceSetIsPlaying { is_playing } => {
                        source.is_playing = is_playing;
                        main_msg_tx.push(MainThreadMessage::SourceSnapshot { is_current_buffer_complete: source.is_curr_buffer_complete, is_playing: source.is_playing }).expect("audio: can't push ss msg");
                    },

                    AudioThreadMessage::BiquadSetF0 { f0 } => {
                        biquad.f0 = f0;
                        main_msg_tx.push(MainThreadMessage::BiquadSnapshot { f0: biquad.f0, q: biquad.q }).expect("audio: can't push bs msg");
                    },

                    AudioThreadMessage::BiquadSetQ { q } => {
                        biquad.q = q;
                        main_msg_tx.push(MainThreadMessage::BiquadSnapshot { f0: biquad.f0, q: biquad.q }).expect("audio: can't push bs msg");
                    },
                }
            }

            for frame in data.chunks_mut(num_channels) {
                let is_output_buffer_completes = [
                    output_buffer_idxs[0] >= output_buffers[0].len(),
                    output_buffer_idxs[1] >= output_buffers[1].len(),
                ];

                //  The buffers should be both incomplete or both complete
                assert!(is_output_buffer_completes[0] == is_output_buffer_completes[1]);

                let is_curr_before_complete_before_processing = source.is_curr_buffer_complete;

                if is_output_buffer_completes[0] {
                    process_audio(
                        &mut temp_buffers,
                        &mut output_buffers,
                        &mut biquad_lagged_input_buffers,
                        &mut biquad_lagged_output_buffers,
                        sample_rate,
                        &mut source,
                        &biquad,
                    );
                    output_buffer_idxs = [ 0, 0 ];
                }

                if source.is_curr_buffer_complete && (!is_curr_before_complete_before_processing) {
                    main_msg_tx.push(MainThreadMessage::SourceSnapshot { is_current_buffer_complete: source.is_curr_buffer_complete, is_playing: source.is_playing }).expect("audio: can't push ss msg");
                }

                for (chan, sample) in frame.iter_mut().enumerate() {
                    let x: f32 = output_buffers[chan][output_buffer_idxs[chan]];
                    output_buffer_idxs[chan] += 1;
                    *sample = T::from_sample(x);
                }
            }
        },
        move |err| {
            eprintln!("{:?}", err);
        },
        None
    ).map_err(|_| ());

    stream
}



/*  Deleted make_stream(), because it is no longer compatible with the current
    architecture where there is no (buggy) synchronization between the audio
    thread and the main thread. */



/// Returns sample size and thread handle.
fn setup_audio(
    audio_msg_rx: Consumer<AudioThreadMessage>,
    main_msg_tx: Producer<MainThreadMessage>
) -> Result<Stream, ()> {
    let host = cpal::default_host();
    
    let device = host.default_output_device().unwrap();

    let config = device.default_output_config().unwrap();

    let sample_format = config.sample_format();
    eprintln!("Sample format: {:?}", sample_format);

    let config: StreamConfig = config.into();
    eprintln!("Buffer size: {:?}", config.buffer_size);
    
    let num_channels = config.channels as usize;
    eprintln!("DAC number of channels: {:?}", num_channels);

    let sample_rate = config.sample_rate.0 as usize;
    eprintln!("Sample rate: {:?}", sample_rate);

    let stream = match sample_format {
        cpal::SampleFormat::I8  => make_stream_biquad_limiter::<i8> (&device, &config, audio_msg_rx, main_msg_tx),
        cpal::SampleFormat::I16 => make_stream_biquad_limiter::<i16>(&device, &config, audio_msg_rx, main_msg_tx),
        cpal::SampleFormat::I32 => make_stream_biquad_limiter::<i32>(&device, &config, audio_msg_rx, main_msg_tx),
        cpal::SampleFormat::I64 => make_stream_biquad_limiter::<i64>(&device, &config, audio_msg_rx, main_msg_tx),
        cpal::SampleFormat::U8  => make_stream_biquad_limiter::<u8> (&device, &config, audio_msg_rx, main_msg_tx),
        cpal::SampleFormat::U16 => make_stream_biquad_limiter::<u16>(&device, &config, audio_msg_rx, main_msg_tx),
        cpal::SampleFormat::U32 => make_stream_biquad_limiter::<u32>(&device, &config, audio_msg_rx, main_msg_tx),
        cpal::SampleFormat::U64 => make_stream_biquad_limiter::<u64>(&device, &config, audio_msg_rx, main_msg_tx),
        cpal::SampleFormat::F32 => make_stream_biquad_limiter::<f32>(&device, &config, audio_msg_rx, main_msg_tx),
        cpal::SampleFormat::F64 => make_stream_biquad_limiter::<f64>(&device, &config, audio_msg_rx, main_msg_tx),
        _ => panic!(),
    };

    stream.and_then(|stream| {
        match stream.play() {
            Ok(_) => Ok(stream),
            Err(_) => Err(()),
        }
    })
}



fn load_sound_from_path<P: AsRef<Path>>(path: P) -> Result<[Vec<f32>; 2], ()> {
    let file = File::open(path).expect("failed to open media");
    let mss = MediaSourceStream::new(Box::new(file), Default::default());
    
    let hint = Hint::new();
    // hint.with_extension(path.extension());

    let meta_opts: MetadataOptions = Default::default();
    let mut fmt_opts: FormatOptions = Default::default();
    fmt_opts.enable_gapless = true;

    let probed = symphonia::default::get_probe()
        .format(&hint, mss, &fmt_opts, &meta_opts)
        .expect("unsupported format");

    let mut format = probed.format;

    let track = format
        .tracks().iter().find(|t| t.codec_params.codec != CODEC_TYPE_NULL)
        .expect("no supported audio tracks");

    // eprintln!("Track codec params:\n{:?}", track.codec_params)  ;

    let dec_opts: DecoderOptions = Default::default();

    let mut decoder = symphonia::default::get_codecs()
        .make(&track.codec_params, &dec_opts)
        .expect("unsupported codec");

    let track_id = track.id;



    let mut packet_buf = None;
    let mut buf: [Vec<f32>; 2] = [ vec![], vec![] ];

    loop {
        let packet = match format.next_packet() {
            Ok(packet) => packet,
            Err(_e) => {
                // eprintln!("{:?}", e);
                break;
            },
        };

        while !format.metadata().is_latest() {
            format.metadata().pop();
        }

        if packet.track_id() != track_id {
            continue;
        }

        match decoder.decode(&packet) {
            Ok(decoded) => {
                if decoded.frames() == 0 {
                    continue;
                }

                assert!(decoded.spec().channels.count() == 2);

                if let None = packet_buf {
                    packet_buf = Some(AudioBuffer::<f32>::new(decoded.capacity() as u64, *decoded.spec()));
                }

                if let Some(packet_buf) = packet_buf.as_mut() {
                    decoded.convert(packet_buf);
                    buf[0].extend(packet_buf.chan(0));
                    buf[1].extend(packet_buf.chan(1));
                }
            },
            Err(e) => {
                panic!("{:?}", e);
            },
        }
    }

    Ok(buf)
}



/// Convenient function that creates a string filled with `x` spaces.
///
/// Used for padding.
fn spaces(x: usize) -> String {
    std::iter::repeat(' ').take(x).collect()
}



fn render<B: Backend>(
    f: &mut Frame<B>,
    is_playing: bool,
    f0: f32,
    q: f32,
) {
    let frame_width: usize = f.size().width as usize;

    let chunks = Layout::default()
        .direction(Direction::Vertical)
        .constraints([
            Constraint::Min(1),
            Constraint::Length(1),
        ])
        .split(f.size());

        let mut help_text = format!(
            "{}    [p] play/pause  [h/l] f0: {:.0}  [j/k] Q: {:.2}  [q] quit",
            match is_playing {
                true => " PLAYING",
                false => " PAUSED ",
            },
            f0,
            q,
        );
        help_text = format!(
            "{}{}",
            help_text,
            spaces(frame_width.checked_sub(help_text.len()).unwrap_or(0))
        );
    
        let help = Paragraph::new(help_text.bold().reversed());
        f.render_widget(help, chunks[1]);
}



fn main() {
    /*  Load command-line arguments. */
    let args: Args = argh::from_env();

    /*  Load playlists. */
    let mut playlist_file = File::open(&args.playlist).unwrap();
    let mut playlist_string = String::new();
    playlist_file.read_to_string(&mut playlist_string).unwrap();

    let playlist: Playlist = toml::from_str(&playlist_string).unwrap();
    eprintln!("{:?}", playlist);



    /*  Set up message channels and audio thread.  Send first buffer to audio thread. */

    let (main_msg_tx, mut main_msg_rx) = RingBuffer::new(MSG_RINGBUFFER_SIZE);
    let (mut audio_msg_tx, audio_msg_rx) = RingBuffer::new(MSG_RINGBUFFER_SIZE);

    let mut sound_idx: usize = 0;
    if let Ok(buffer) = load_sound_from_path(&playlist.paths[sound_idx]) {
        audio_msg_tx.push(AudioThreadMessage::SourceInsertBuffer { key: sound_idx, buffer }).expect("main thread: can't push first sib msg");
        audio_msg_tx.push(AudioThreadMessage::SourceSetCurrentBufferKey { key: sound_idx }).expect("main thread: can't push first scbk msg");
        audio_msg_tx.push(AudioThreadMessage::SourceSetIsPlaying { is_playing: true }).expect("main thread: can't push first sip msg");
    }

    let _stream = setup_audio(audio_msg_rx, main_msg_tx);
    // let sample_rate = sample_rate_rx.recv().unwrap();



    /*  Local representation of modules. */
    let mut source_is_playing = false;
    let mut biquad_f0: f32 = 800.;
    let mut biquad_q: f32 = 0.7;



    //  Terminal stuff
    crossterm::terminal::enable_raw_mode().unwrap();
    crossterm::execute!(io::stderr(), crossterm::terminal::EnterAlternateScreen).unwrap();

    //  Initialize terminal backend
    let mut terminal = Terminal::new(CrosstermBackend::new(io::stderr())).unwrap();

    loop {
        /*  Check for messages. */
        if let Ok(msg) = main_msg_rx.pop() {
            match msg {
                MainThreadMessage::SourceSnapshot { is_current_buffer_complete, is_playing } => {
                    /*  If current buffer is complete, load next and send. */
                    if is_current_buffer_complete {
                        sound_idx += 1;
                        if sound_idx >= playlist.paths.len() {
                            break;
                        }
            
                        if let Ok(buffer) = load_sound_from_path(&playlist.paths[sound_idx]) {
                            audio_msg_tx.push(AudioThreadMessage::SourceRemoveBuffer { key: sound_idx - 1 }).expect("main: can't push srb msg");
                            audio_msg_tx.push(AudioThreadMessage::SourceInsertBuffer { key: sound_idx, buffer }).expect("main: can't push sib msg");
                            audio_msg_tx.push(AudioThreadMessage::SourceSetCurrentBufferKey { key: sound_idx }).expect("main: can't push scbk msg");
                            audio_msg_tx.push(AudioThreadMessage::SourceSetCurrentBufferSampleIdxs { idxs: [ 0, 0 ] }).expect("main: can't push scbsi msg");
                            audio_msg_tx.push(AudioThreadMessage::SourceSetIsPlaying { is_playing: true }).expect("main: can't push sip msg");
                        }
                    }
                    source_is_playing = is_playing;
                },

                MainThreadMessage::BiquadSnapshot { f0, q } => {
                    biquad_f0 = f0;
                    biquad_q = q;
                },
            }
        }

        /*  Render UI. */
        terminal.draw(|f| render(f, source_is_playing, biquad_f0, biquad_q)).unwrap();

        //  React to event
        if let Ok(true) = event::poll(Duration::from_millis(0)) {
            if let Ok(Event::Key(key)) = event::read() {
                if key.kind == event::KeyEventKind::Release {
                    continue;
                }

                match key.code {
                    KeyCode::Char('p') => {
                        let new_source_is_playing = !source_is_playing;
                        audio_msg_tx.push(AudioThreadMessage::SourceSetIsPlaying { is_playing: new_source_is_playing }).expect("main: can't push sip msg");
                        source_is_playing = new_source_is_playing;
                    },

                    KeyCode::Char('h') => {
                        let new_biquad_f0 = match biquad_f0 <= 1000. {
                            true => BIQUAD_F0_MIN.max(biquad_f0 - 100.),
                            false => biquad_f0 - 1000.
                        };
                        audio_msg_tx.push(AudioThreadMessage::BiquadSetF0 { f0: new_biquad_f0 }).expect("main: can't push bsf msg");
                        biquad_f0 = new_biquad_f0;
                    },

                    KeyCode::Char('l') => {
                        let new_biquad_f0 = match biquad_f0 >= 1000. {
                            true => BIQUAD_F0_MAX.min(biquad_f0 + 1000.),
                            false => biquad_f0 + 100.
                        };
                        audio_msg_tx.push(AudioThreadMessage::BiquadSetF0 { f0: new_biquad_f0 }).expect("main: can't push bsf msg");
                        biquad_f0 = new_biquad_f0;
                    },

                    KeyCode::Char('j') => {
                        let new_biquad_q = BIQUAD_Q_MIN.max(biquad_q - 0.1);
                        audio_msg_tx.push(AudioThreadMessage::BiquadSetQ { q: new_biquad_q }).expect("main: can't push bsq msg");
                        biquad_q = new_biquad_q;
                    },

                    KeyCode::Char('k') => {
                        let new_biquad_q = BIQUAD_Q_MAX.min(biquad_q + 0.1);
                        audio_msg_tx.push(AudioThreadMessage::BiquadSetQ { q: new_biquad_q }).expect("main: can't push bsq msg");
                        biquad_q = new_biquad_q;
                    },

                    KeyCode::Char('q') => {
                        break;
                    },
                    _ => todo!(),
                }
            }
        }

        std::thread::sleep(Duration::from_millis(MAIN_LOOP_SLEEP_DUR));
    }

    //  Clean up terminal
    crossterm::execute!(io::stderr(), crossterm::terminal::LeaveAlternateScreen).unwrap();
    crossterm::terminal::disable_raw_mode().unwrap();
}