# `saisei`

Command-line music player.

Pass a TOML file as a command-line argument.  Example TOML file is `playlist.toml`.

Press p for pause/resume, h/l for prev/next song, j/k for lower/higher volume (adjusts amplitude multiplier by -/+ 0.1), q to quit.

# Design

There are two threads that are running constantly: the main (UI) thread and the audio thread.

## Main thread

1.  Create the following variables, which will be shared with the audio callback:
    1.  A map that stores all audio buffers; the buffers are indexed with some kind of key (currently `usize`).
    2.  A key that indexes the currently playing buffer.
    3.  A bool that indicates whether the currently playing buffer has finished playing.
    4.  A bool that indicates whether the player is playing.
2.  Set up the audio device and provide an audio callback.
2.  Loop:
    1.  If the currently playing buffer has finished playing, then load the next sound file into an audio buffer, insert the buffer into the map with a new key, and set the currently playing buffer key to this key.
    2.  Render UI.
    3.  Check for user input.  If there is user input, respond appropriately.  Exit the loop if the user input was 'q' (quit).
    4.  Sleep for `UI_LOOP_SLEEP_DUR`ms.

## Audio thread (audio callback)

If the player is playing, and there are remaining samples to play from the currently playing buffer, then the audio callback keeps writing these samples to the output buffer.  If there are no samples to write, the audio callback just writes 0.

# DSP

Modules

*   Biquad
*   Limiter

# To-do

There are a lot of race conditions right now because the source buffers and their information (buffer indices, whether buffer is complete or not (do we even need this as a separate info?)) and the player info (whether player is currently playing) are stored separately.  We can't use mutexes in to synchronize all of this info so we should probably store all the buffer-related info inside the map values.

Alternatively, don't synchronize this information at all.  Let the audio callback keep everything; send requests to the audio callback with a lock-free queue.